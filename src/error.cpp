/*
This file is a part  of error.hpp and fs (filesystem) library
*/

#include "error.hpp"

namespace fs
{

void setError(int* err, int code)
{
    if(err)
    {
        *err = code;
    }
}

std::string tostring(int err)
{
    if(err == PERM)
    {
        return std::string("Operation not permitted");
    }
    else if(err == NOENT)
    {
        return std::string("No such file or directory");
    }
    else if(err == IO)
    {
        return std::string("I/O error");
    }
    else if(err == BADF)
    {
        return std::string("Bad file number");
    }
    else if(err == NOMEM)
    {
        return std::string("Out of memory");
    }
    else if(err == ACCES)
    {
        return std::string("Permission denied");
    }
    else if(err == FAULT)
    {
        return std::string("Bad address");
    }
    else if(err == BUSY)
    {
        return std::string("Device or resource busy");
    }
    else if(err == EXIST)
    {
        return std::string("File or Directory exists");
    }
    else if(err == XDEV)
    {
        return std::string("Cross-device link");
    }
    else if(err == NOTDIR)
    {
        return std::string("Not a directory");
    }
    else if(err == ISDIR)
    {
        return std::string("Is a directory");
    }
    else if(err == INVAL)
    {
        return std::string("Invalid argument");
    }
    else if(err == NFILE)
    {
        return std::string("File table overflow");
    }
    else if(err == MFILE)
    {
        return std::string("Too many open files");
    }
    else if(err == NOSPC)
    {
        return std::string("No space left on device");
    }
    else if(err == ROFS)
    {
        return std::string("Read-only file system");
    }
    else if(err == MLINK)
    {
        return std::string("Too many links");
    }
    else if(err == NAMETOOLONG)
    {
        return std::string("File name too long");
    }
    else if(err == NOTEMPTY)
    {
        return std::string("Directory not empty");
    }
    else if(err == LOOP)
    {
        return std::string("Too many symbolic links encountered");
    }
    else if(err == _OVERFLOW)
    {
        return std::string("Value too large for defined data type");
    }
    else if(err == DQUOT)
    {
        return std::string("Quota exceeded");
    }

    /* custom */
    else if(err == NOTFILE)
    {
        return std::string("Not a file");
    }
    else if(err == NOTDEST)
    {
        return std::string("Destination directory not exists");
    }
    else
    {
        return std::string("Unknown error");
    }
}

}


