/*
This file is a part  of utils.hpp and fs (filesystem) library
*/

#include "utils.hpp"

namespace fs
{

bool compareBySize(const FileInfo& first, const FileInfo& second)
{
    if(first.bytes < second.bytes)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool compareByCreated(const FileInfo& first, const FileInfo& second)
{
    if(first.created < second.created)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool compareByPath(const FileInfo& first, const FileInfo& second)
{
    for( std::string::const_iterator lit = first.path.begin(), rit = second.path.begin(); lit != first.path.end() && rit != second.path.end(); ++lit, ++rit )
    {
        if( tolower( *lit ) < tolower( *rit ) )
        {
            return true;
        }
        else if( tolower( *lit ) > tolower( *rit ) )
        {
            return false;
        }
    }
    if( first.path.size() < second.path.size() )
    {
        return true;
    }
    return false;
}

bool compareByName(const FileInfo* first, const FileInfo* second)
{
    for( std::string::const_iterator lit = first->name.begin(), rit = second->name.begin(); lit != first->name.end() && rit != second->name.end(); ++lit, ++rit )
    {
        if( tolower( *lit ) < tolower( *rit ) )
        {
            return true;
        }
        else if( tolower( *lit ) > tolower( *rit ) )
        {
            return false;
        }
    }
    if( first->name.size() < second->name.size() )
    {
        return true;
    }
    return false;
}

std::string formatSizeUnits(size_t bytes)
{
    std::string value;
    if (bytes >= 1073741824)
    {
        value.assign(std::to_string(bytes / 1073741824)).append(" GB");
    }
    else if (bytes >= 1048576)
    {
        value.assign(std::to_string(bytes / 1048576)).append(" MB");
    }
    else if (bytes >= 1024)
    {
        value.assign(std::to_string(bytes / 1024)).append(" kB");
    }
    else if (bytes > 1)
    {
        value.assign(std::to_string(bytes)).append(" bytes");
    }
    else if (bytes == 1)
    {
        value.assign(std::to_string(bytes)).append(" byte");
    }
    else
    {
        value.assign("0 bytes");
    }

    return value;
}

}



