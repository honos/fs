/*
This file is a part  of path.hpp and fs (filesystem) library
*/

#include "path.hpp"
#include "dir.hpp"

#include <string>
#include <vector>
#include <stdexcept>
#include <sstream>
#include <cctype>
#include <cstdlib>
#include <cerrno>
#include <cstring>

# include <unistd.h>
#include <sys/stat.h>

# include <linux/limits.h>

#include <exception>

namespace fs
{

Path::Path():
    m_absolute(false)
{}

Path::Path(const Path &path):
    m_path(path.m_path),
    m_absolute(path.m_absolute)
{}

Path::Path(Path &&path):
    m_path(std::move(path.m_path)),
    m_absolute(path.m_absolute)
{}

Path::Path(const char *string)
{
    set(string);
}

Path::Path(const std::string &string)
{
    set(string);
}

size_t Path::length()
{
    return m_path.size();
}

bool Path::is_absolute()
{
    return m_absolute;
}

void Path::make_absolute()
{
    m_absolute = true;
}

void Path::make_relative()
{
    m_absolute = false;
}

void Path::normalize()
{
    char temp[PATH_MAX];
    if (realpath(str().c_str(), temp) == NULL)
    {
        int i = 0;
        for(std::string it: m_path)
        {
            if(it.compare("..") == 0)
            {
                std::vector<std::string>::iterator itr = m_path.begin() + i;
                m_path.erase(itr);
                if(i > 0)
                {
                    m_path.erase(itr - 1);
                }
            }
            else if(it.compare(".") == 0)
            {
                std::vector<std::string>::iterator itr = m_path.begin() + i;
                m_path.erase(itr);
            }
            i++;
        }
    }
    else
    {
        set(temp);
    }
}

std::string Path::extension()
{
    const std::string &name = filename();
    size_t pos = name.find_last_of(".");
    if (pos == std::string::npos)
        return "";
    return name.substr(pos+1);
}

std::string Path::filename()
{
    if (m_path.empty())
        return "";
    const std::string &last = m_path[m_path.size()-1];
    return last;
}

Path Path::remove_filename()
{
    return parent_path();
}


Path Path::parent_path()
{
    Path result;
    result.m_absolute = m_absolute;

    if (m_path.empty())
    {
        if (!m_absolute)
            result.m_path.push_back("..");
    }
    else
    {
        size_t until = m_path.size() - 1;
        for (size_t i = 0; i < until; ++i)
            result.m_path.push_back(m_path[i]);
    }
    return result;
}

Path Path::operator+(const Path &other)
{
    if (other.m_absolute)
    {
        throw std::runtime_error("Path::operator+(): expected a relative path!");
    }

    Path result(*this);

    for (size_t i=0; i<other.m_path.size(); ++i)
    {
        result.m_path.push_back(other.m_path[i]);
    }
    return result;
}

std::string Path::str()
{
    std::ostringstream oss;

    if (m_absolute)
    {
        oss << "/";
    }
    for (size_t i=0; i<m_path.size(); ++i)
    {
        oss << m_path[i];
        if (i+1 < m_path.size())
        {
            oss << '/';
        }
    }

    return oss.str();
}

void Path::set(const std::string &str)
{
    m_path = tokenize(str);
    m_absolute = !str.empty() && str[0] == '/';
}

Path& Path::operator=(const Path &path)
{
    m_path = path.m_path;
    m_absolute = path.m_absolute;
    return *this;
}

Path& Path::operator=(Path &&path)
{
    if (this != &path)
    {
        m_path = std::move(path.m_path);
        m_absolute = path.m_absolute;
    }
    return *this;
}

std::ostream& Path::operator<<(std::ostream &os)
{
    os << str();
    return os;
}

bool Path::operator==(const Path &p)
{
    return p.m_path == m_path;
}
bool Path::operator!=(const Path &p)
{
    return p.m_path != m_path;
}

std::vector<std::string>::iterator Path::begin()
{
    return m_path.begin();
}

std::vector<std::string>::iterator Path::end()
{
    return m_path.end();
}


std::vector<std::string> Path::tokenize(const std::string &string)
{
    std::string::size_type lastPos = 0, pos = string.find_first_of("/", lastPos);
    std::vector<std::string> tokens;

    while (lastPos != std::string::npos)
    {
        if (pos != lastPos)
        {
            tokens.push_back(string.substr(lastPos, pos - lastPos));
        }
        lastPos = pos;
        if (lastPos == std::string::npos || lastPos + 1 == string.length())
        {
            break;
        }
        pos = string.find_first_of("/", ++lastPos);
    }

    return tokens;
}

}

