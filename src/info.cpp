/*
This file is a part  of info.hpp and fs (filesystem) library
*/

#include "info.hpp"
#include "error.hpp"

#include <sys/stat.h>
#include <iostream>


namespace fs
{

FileInfo* getInfo(Path path, int* err)
{
    struct stat sb;
    if (stat(path.str().c_str(), &sb) == -1)
    {
        setError(err, errno);
        return nullptr;
    }

    FileInfo* fi = new FileInfo;
    fi->path = path.str();
    fi->name = path.filename();

    // overeni typu
    {
        mode_t val=(sb.st_mode & S_IFMT);
        if(val & S_IFDIR) // directory
        {
            fi->type = Type::DIRECTORY;
        }
        else if(val & S_IFREG) // regular file
        {
            fi->type = Type::FILE;
        }
        else if(val & S_IFBLK) // block device
        {
            fi->type = Type::DEVICE;
        }
        else if(val & S_IFCHR) // character device
        {
            fi->type = Type::OTHER;
        }
        else if(val & S_IFIFO) // FIFO/pipe
        {
            fi->type = Type::OTHER;
        }
        else if(val & S_IFLNK) // symlink
        {
            fi->type = Type::OTHER;
        }
        else if(val & S_IFSOCK) // socket
        {
            fi->type = Type::OTHER;
        }
        else // unknown
        {
            fi->type = Type::UNKNOWN;
        }

    }

    // opravneni
    {
        mode_t val=(sb.st_mode & S_IFMT);

        // user
        {
            if(val & S_IRUSR)
            {
                fi->uPerm |= Perm::READ;
            }

            if(val & S_IWUSR)
            {
                fi->uPerm |= Perm::WRITE;
            }

            if(val & S_IXUSR)
            {
                fi->uPerm |= Perm::EXEC;
            }
        }

        // group
        {
            if(val & S_IRGRP)
            {
                fi->gPerm |= Perm::READ;
            }

            if(val & S_IWGRP)
            {
                fi->gPerm |= Perm::WRITE;
            }

            if(val & S_IXGRP)
            {
                fi->gPerm |= Perm::EXEC;
            }
        }

        // root
        {
            if(val & S_IROTH)
            {
                fi->rPerm |= Perm::READ;
            }

            if(val & S_IWOTH)
            {
                fi->rPerm |= Perm::WRITE;
            }

            if(val & S_IXOTH)
            {
                fi->rPerm |= Perm::EXEC;
            }
        }
    }


    // vlastnik
    fi->uid = (long) sb.st_uid;
    fi->gid = (long) sb.st_gid;

    // velikost v bytes
    fi->bytes = sb.st_size;

    // casy
    fi->created = sb.st_ctime;
    fi->access = sb.st_atime;
    fi->modified = sb.st_mtime;

    return fi;
}

}
