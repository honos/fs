/*
This file is a part  of dir.hpp and fs (filesystem) library
*/

#include "dir.hpp"
#include "path.hpp"
#include "file.hpp"
#include "error.hpp"

#include <sys/stat.h>
#include <dirent.h>
#include <iostream>

namespace fs
{

FileInfo* dirCreate(Path path, bool recursive, int* err)
{
    if(dirExists(path, err))
    {
        setError(err, Error::EXIST);
        return nullptr;
    }

    if(fileExists(path))
    {
        setError(err, Error::NOTDIR);
        return nullptr;
    }

    if(recursive)
    {
        FileInfo* fi;
        Path p;
        for(std::vector<std::string>::iterator dir = path.begin() ; dir != path.end(); ++dir)
        {
            p = p + Path(*dir);
            p.make_absolute();
            if(dirExists(Path(p), err))
            {
                continue;
            }
            fi = dirCreate(p, false, err);
            if(!fi)
            {
                return nullptr;
            }
        }
        return getInfo(p);
    }
    else
    {
        int ec = mkdir(path.str().c_str(), S_IRUSR | S_IWUSR | S_IXUSR);
        if(ec == 0)
        {
            return getInfo(path);
        }
        else
        {
            setError(err, errno);
            return nullptr;
        }
    }

}

DirectoryContent* dirRead(FileInfo* dir, bool hidden, bool dots, int* err)
{
    DirectoryContent* content = new DirectoryContent;
    DIR* directory = opendir(dir->path.c_str());
    struct dirent *dirent_ptr;
    if (!directory)
    {
        setError(err, errno);
        return content;
    }


    while ((dirent_ptr = readdir(directory)) != nullptr)
    {
        if (dirent_ptr->d_type == DT_REG)
        {
            if(!hidden && dirent_ptr->d_name[0] == '.')
            {
                continue;
            }

            FileInfo* fileInfo = getInfo(Path(dir->path) + Path(dirent_ptr->d_name));
            content->files.push_back(fileInfo);
        }
        else if (dirent_ptr->d_type == DT_DIR)
        {
            if(!dots && (dirent_ptr->d_name[0] == '.' || (dirent_ptr->d_name[0] == '.' && dirent_ptr->d_name[1] == '.')))
            {
                continue;
            }
            if(!hidden && dirent_ptr->d_name[0] == '.')
            {
                continue;
            }


            FileInfo* fileInfo = getInfo(Path(dir->path) + Path(dirent_ptr->d_name));
            content->dirs.push_back(fileInfo);
        }
        else
        {
            FileInfo* fileInfo = getInfo(Path(dir->path) + Path(dirent_ptr->d_name));
            content->others.push_back(fileInfo);
        }
    }
    closedir(directory);
    return content;
}

FileInfo* dirRename(FileInfo* dir, const std::string& name, int* err)
{
    if(dir->type != Type::DIRECTORY)
    {
        setError(err, Error::NOTDIR);
        return nullptr;
    }

    Path new_file(Path(dir->path).parent_path() + Path(name));
    if(std::rename(dir->path.c_str(), new_file.str().c_str()) == 0)
    {
        return getInfo(new_file);
    }
    else
    {
        setError(err, errno);
        return nullptr;
    }
}

FileInfo* dirCopy(FileInfo* dir, Path to, bool rewrite, int* err)
{
    struct dirent *entry;

    DIR* dp = opendir(dir->path.c_str());
    if (dp == NULL)
    {
        setError(err, errno);
        return nullptr;
    }

    if(dirCreate(to, false, err) == nullptr)
    {
        closedir(dp);
        return nullptr;
    }

    while ((entry = readdir(dp)))
    {
        if(entry->d_type == DT_DIR)
        {
            if(entry->d_name[0] == '.' || (entry->d_name[0] == '.' && entry->d_name[1] == '.' ))
            {
                continue;
            }

            FileInfo* fi = getInfo(Path(std::string(dir->path).append("/").append(entry->d_name)));
            if(dirCopy(fi, to + Path(entry->d_name), false, err) == nullptr)
            {
                closedir(dp);
                return nullptr;
            }
        }
        else if(entry->d_type == DT_REG)
        {
            FileInfo* fi_old = getInfo(Path(dir->path) + Path(entry->d_name));
            if(!fi_old)
            {
                closedir(dp);
                return nullptr;
            }
            if(fileCopy(fi_old, to + Path(entry->d_name), false, err) == nullptr)
            {
                closedir(dp);
                return nullptr;
            }
        }
    }

    closedir(dp);
    return getInfo(to);
}

FileInfo* dirMove(FileInfo* dir, Path to, bool rewrite, int* err)
{
    struct dirent *entry;

    DIR* dp = opendir(dir->path.c_str());
    if (dp == NULL)
    {
        setError(err, errno);
        return nullptr;
    }

    to.normalize();

    if(dirCreate(to, false, err) == nullptr)
    {
        closedir(dp);
        return nullptr;
    }

    while ((entry = readdir(dp)))
    {
        if(entry->d_type == DT_DIR)
        {
            if(entry->d_name[0] == '.' || (entry->d_name[0] == '.' && entry->d_name[1] == '.' ))
            {
                continue;
            }

            FileInfo* fi = getInfo(Path(std::string(dir->path).append("/").append(entry->d_name)));
            if(dirMove(fi, to + Path(entry->d_name), false, err) == nullptr)
            {
                closedir(dp);
                return nullptr;
            }
        }
        else if(entry->d_type == DT_REG)
        {
            FileInfo* fi_old = getInfo(Path(dir->path) + Path(entry->d_name));
            if(!fi_old)
            {
                closedir(dp);
                return nullptr;
            }
            if(fileMove(fi_old, to, false, err) == nullptr)
            {
                closedir(dp);
                return nullptr;
            }
            fileDelete(fi_old, err);
        }
    }

    closedir(dp);
    dirDelete(dir, false, err);
    return getInfo(to);
}


bool dirDelete(FileInfo* dir, bool recursive, int* err)
{
    DIR *_dir;
    struct dirent *entry;

    _dir = opendir(dir->path.c_str());
    if (dir == NULL)
    {
        setError(err, errno);
        return false;
    }

    while ((entry = readdir(_dir)) != NULL)
    {
        if (strcmp(entry->d_name, ".") && strcmp(entry->d_name, ".."))
        {
            Path path = Path(dir->path) + Path(entry->d_name);
            FileInfo* fi = getInfo(path); // zde pridat &err
            if(!fi)
            {
                closedir(_dir);
                return false;
            }

            if (fi->type == Type::DIRECTORY)
            {
                dirDelete(fi, true, err);
            }

            if(remove(fi->path.c_str()) != 0)
            {
                setError(err, errno);
                closedir(_dir);
                return false;
            }
        }

    }

    if(remove(dir->path.c_str()) != 0)
    {
        setError(err, errno);
        closedir(_dir);
        return false;
    }

    closedir(_dir);
    return true;
}

bool dirExists(Path path, int* err)
{
    struct stat sb;
    if(stat(path.str().c_str(), &sb) == 0)
    {
        if(sb.st_mode & S_IFDIR)
        {
            return true;
        }
        else
        {
            setError(err, Error::NOTDIR);
            return false;
        }
    }
    else
    {
        if(errno != ENOENT)
        {
            setError(err, errno);
        }
        return false;
    }
}

bool dirEmpty(Path path, int* err)
{
    int n = 0;
    struct dirent *d;
    DIR *dir = opendir(path.str().c_str());
    if (dir == NULL)
    {
        setError(err, errno);
        return false;
    }
    while ((d = readdir(dir)) != NULL)
    {
        if(++n > 2)
        {
            break;
        }
    }
    closedir(dir);
    if (n <= 2)
    {
        return true;
    }
    else
    {
        return false;
    }
}


}
