/*
This file is a part  of file.hpp and fs (filesystem) library
*/

#include "file.hpp"
#include "dir.hpp"
#include "error.hpp"

#include <sys/stat.h>
#include <fstream>
#include <iostream>


namespace fs
{

FileInfo* fileCreate(Path path, const std::string& name, bool recreate, int* err)
{
    if(!dirExists(path))
    {
        setError(err, Error::NOENT);
        return nullptr;
    }

    Path new_path(path + Path(name));
    {
        bool file_exists = fileExists(new_path, err);
        if(recreate && file_exists)
        {
            if(!fileDelete(getInfo(new_path), err))
            {
                return nullptr;
            }
            file_exists = false;
        }

        if(file_exists)
        {
            setError(err, Error::EXIST);
            return nullptr;
        }
    }

    std::fstream fs(new_path.str().c_str(), std::ios::out);
    if(fs)
    {
        fs.close();
        setError(err, errno);
        return getInfo(new_path);
    }
    else
    {
        setError(err, errno);
        return nullptr;
    }
}

FileInfo* fileWrite(Path path, const std::string& content, bool create, int* err)
{
    if(!create && !fileExists(path, err))
    {
        setError(err, Error::NOENT);
        return nullptr;
    }
    std::ofstream output(path.str());
    if(output)
    {
        output << content;
        output.close();
    }
    else
    {
        return nullptr;
    }
    return getInfo(path);
}

std::string fileRead(FileInfo* file, int* err)
{
    std::stringstream ss;
    std::ifstream input(file->path);
    if(input)
    {
        ss << input.rdbuf();
        input.close();
    }
    else
    {
        setError(err, errno);
    }
    return ss.str();
}

std::vector<std::string> fileReadLines(FileInfo* file, int* err)
{
    std::vector<std::string> res;
    std::ifstream input(file->path);
    if (input)
    {
        std::string line;
        while(getline(input, line))
        {
            res.emplace_back(line);
        }
        input.close();
    }
    else
    {
        setError(err, errno);
    }
    return res;
}

FileInfo* fileCopy(FileInfo* file, Path to, bool rewrite, int* err)
{
    if(file->type != Type::FILE)
    {
        setError(err, Error::NOTFILE);
        return nullptr;
    }

    std::fstream fin(file->path.c_str(), std::ios::in | std::ios::binary);
    if(!fin)
    {
        setError(err, errno);
        return nullptr;
    }

    std::fstream fout(to.str().c_str(), std::ios::out | std::ios::binary);
    if(!fout)
    {
        setError(err, errno);
        fin.close();
        return nullptr;
    }

    char c;
    while(!fin.eof())
    {
        fin.get(c);
        fout.put(c);
    }
    fin.close();
    fout.close();

    return getInfo(to);
}

FileInfo* fileRename(FileInfo* file, const std::string& name, int* err)
{
    if(file->type != Type::FILE)
    {
        setError(err, Error::NOTFILE);
        return nullptr;
    }

    Path new_file(Path(file->path).parent_path() + Path(name));
    if(std::rename(file->path.c_str(), new_file.str().c_str()) == 0)
    {
        return getInfo(new_file);
    }
    else
    {
        setError(err, errno);
        return nullptr;
    }
}

FileInfo* fileMove(FileInfo* file, Path to, bool rewrite, int* err)
{
    if(file->type != Type::FILE)
    {
        setError(err, Error::NOTFILE);
        return nullptr;
    }

    if(!dirExists(to, err))
    {
        setError(err, Error::NOTDEST);
        return nullptr;
    }

    Path new_file(to + Path(file->name));
    if(std::rename(file->path.c_str(), new_file.str().c_str()) == 0)
    {
        return getInfo(new_file);
    }
    else
    {
        setError(err, errno);
        return nullptr;
    }
}

bool fileDelete(FileInfo* file, int* err)
{
    if(std::remove(file->path.c_str()) == 0)
    {
        return true;
    }
    else
    {
        setError(err, errno);
        return false;
    }
}

bool fileExists(Path path, int* err)
{
    struct stat sb;
    if(stat(path.str().c_str(), &sb) == 0)
    {
        return sb.st_mode & S_IFREG;
    }
    else
    {
        if(errno != ENOENT)
        {
            setError(err, errno);
        }
        return false;
    }
}


}



















