/*
    Copyright (c) 2017, Denis Colesnicov <eugustus@gmail.com>
    All rights reserved.
    New BSD License can be found in the LICENSE file.
*/

#pragma once

#include "info.hpp"
#include "path.hpp"

#include <string>
#include <vector>

namespace fs
{


/**
* Vytvorit novy prazdny soubor
*
* @param    path Cesta kde ho vytvorit (slozka)
* @param    name Nazev souboru
* @param    create Pokud soubor existuje smazat ho a vytvorit novy?
* @param    err Ukazatel na kod chyby @see errno.hpp
*
* @return   FileInfo* Ukazatel na strukturu informaci o souboru nebo nullptr pokud se nepovedlo
*/
FileInfo* fileCreate(Path path, const std::string& name, bool recreate = false, int* err = nullptr);

/**
* Zapise text do souboru
*
* @param    file Ukazatel na strukturu informaci o souboru
* @param    content "Text" pro zapsani
* @param    create Pokud soubor neexistuje vytvorit ho?
* @param    err Ukazatel na kod chyby @see errno.hpp
*
* @return   FileInfo* Ukazatel na strukturu informaci o souboru nebo nullptr pokud se nepovedlo
*/
FileInfo* fileWrite(Path file, const std::string &content, bool create = false, int* err = nullptr);

/**
* Precte obsah souboru
*
* @param    file Ukazatel na strukturu informaci o souboru
* @param    err Ukazatel na kod chyby @see errno.hpp
*
* @return   std::string Obsah souboru
*/
std::string fileRead(FileInfo* file, int* err = nullptr);

/**
* Precte radky ze souboru
*
* @param    file Ukazatel na strukturu informaci o souboru
* @param    err Ukazatel na kod chyby @see errno.hpp
*
* @return   std::vector<std::string> Prectene radky
*/
std::vector<std::string> fileReadLines(FileInfo* file, int* err = nullptr);

/**
* Zkopiruje soubor so doveho umisteni
*
* @param    file Ukazatel na strukturu informaci o souboru
* @param    to Cil kam kopirovat (slozka)
* @param    rewrite pokud v destinaci existuje soubor se stejnym nazvem tak ho smazat?
* @param    err Ukazatel na kod chyby @see errno.hpp
*
* @return   FileInfo* Ukazatel na prejmenovany soubor, jinak nullptr pokud se nepovedlo
*/
FileInfo* fileCopy(FileInfo* file, Path to, bool rewrite = false, int* err = nullptr);

/**
* Prejmenuje soubor
*
* @param    file Ukazatel na strukturu informaci o souboru
* @param    name Novy nazev souboru
* @param    err Ukazatel na kod chyby @see errno.hpp @see error.hpp
*
* @return   FileInfo* Ukazatel na prejmenovany soubor, jinak nullptr pokud se nepovedlo
*/
FileInfo* fileRename(FileInfo* file, const std::string& name, int* err = nullptr);

/**
* Presune soubor
*
* @param    file Ukazatel na strukturu informaci o souboru
* @param    to Cil kam presunout (slozka)
* @param    rewrite Pokud v destinaci existuje soubor se stejnym nazvem tak ho prepsat?
* @param    err Ukazatel na kod chyby @see errno.hpp
*
* @return   FileInfo* Ukazatel na presunuty soubor, jinak nullptr pokud se nepovedlo
*/
FileInfo* fileMove(FileInfo* file, Path to, bool rewrite = false, int* err = nullptr);

/**
* Smaze soubor
*
* @param    file Ukazatel na strukturu informaci o souboru
* @param    err Ukazatel na kod chyby @see errno.hpp
*
* @return   TRUE pokud se povedlo, jinak FALSE
*/
bool fileDelete(FileInfo* file, int* err = nullptr);

/**
* Overi existenci souboru
*
* @param    path cesta k souboru
* @param    err Ukazatel na kod chyby @see errno.hpp
*
* @return   TRUE pokud existuje, jinak FALSE
*
* @todo     Kdyz dojde ke smazani, mel by se i odstranit ukazatel na FileInfo*!!
*/
bool fileExists(Path path, int* err = nullptr);


}


