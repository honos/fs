/*
    Copyright (c) 2017, Denis Colesnicov <eugustus@gmail.com>
    All rights reserved.
    New BSD License can be found in the LICENSE file.
*/

#pragma once


#include "path.hpp"
#include "info.hpp"
#include "file.hpp"
#include "dir.hpp"
#include "utils.hpp"
#include "error.hpp"


/**
* Funkce, struktury a definice pro praci se souborovym systemem
*
* @author Denis Colesnicov
*/
namespace fs {}
