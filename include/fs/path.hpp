/*
    Copyright (c) 2017, Denis Colesnicov <eugustus@gmail.com>
    All rights reserved.
    New BSD License can be found in the LICENSE file.
*/

#pragma once

//#include "fwd.h"
#include <string>
#include <vector>
#include <stdexcept>
#include <sstream>
#include <cctype>
#include <cstdlib>
#include <cerrno>
#include <cstring>

#if defined(_WIN32)
# include <windows.h>
#else
# include <unistd.h>
#endif
#include <sys/stat.h>

#if defined(__linux)
# include <linux/limits.h>
#endif



namespace fs
{

/**
 * Reprezentuje cesty v souborovem systemu pro LINIX
 */
class Path
{
public:

    Path();

    /**
    * @param    &path
    */
    Path(const Path &path);

    /**
    *
    *
    * @param    &&path
    */
    Path(Path &&path);

    /**
    *
    *
    * @param    *string
    */
    Path(const char *string);

    /**
    * @param    &string
    */
    Path(const std::string &string);

    /**
    * Z kolika segmentu se sklada cesta?
    *
    * @return   size_t POcet segmentu
    */
    size_t length();


    /**
    * Jedna se o absolutni cestu (zacina lomitkem '/')?
    *
    * @return   TRUE pokud je absolutni, jinak FALSE
    */
    bool is_absolute();

    /**
    * Udela z cesty absolutni (cesta bude zacinat lomitkem '/')
    */
    void make_absolute();

    /**
    * Udela z cesty relativni (cesta nebude zacinat lomitkem '/')
    */
    void make_relative();

    /**
    * Normalizuje cestu
    *
    * @details  Odstrani z cesty '.' a '..'
    */
    void normalize();

    /**
    * Vrati koncovku souboru. Pokud soubor nema koncovku, vrati prazdny retezec
    *
    * @note     Neoveruje jestli cesta vede na existujici cil
    * @note     Nerozdeluje jestli se jedna o slozku nebo soubor
    * @note     Nerozdeluje jestli se jedna o slozku nebo soubor
    *
    * @return   std::string Koncovka souboru
    */
    std::string extension();

    /**
    * Vrati nazev souboru. Pokud se jedna o slozku, vrati prazdny retezec
    *
    * @see  parent_path()
    *
    * @note     Neoveruje jestli cesta vede na existujici cil
    * @note     Nerozdeluje jestli se jedna o slozku nebo soubor
    *
    * @return   std::string Nazev souboru
    */
    std::string filename();

    /**
    * Vrati cestu k nadrazene slozce
    *
    * @see  filename()
    *
    * @note     Neoveruje jestli cesta vede na existujici cil
    * @note     Nerozdeluje jestli se jedna o slozku nebo soubor
    *
    * @return   Path Cesta k nadrazene slozce
    */
    Path parent_path();

    /**
    * Odstrani nazev souboru z cesty
    *
    * @details  Pokud se nejedna o soubor, je nastaven kod chyby
    *
    * @see  parent_path()
    *
    * @note     Pokud se jedna o cestu ke slozce, odstrani nazev posledni slozky
    * @note     Neoveruje jestli cesta vede na existujici cil
    * @note     Nerozdeluje jestli se jedna o slozku nebo soubor
    *
    * @return   Path Cesta bez nazvu souboru (Cesta ke slozce, ve ktere se nachazi soubor)
    */
    Path remove_filename();

    /**
    * Vrati retezec reprezentujici cestu
    *
    * @return   std::string Retezec reprezentujici cestu
    */
    std::string str();

    /**
    * Nastavi cestu (provede tokenizaci, zjisti jestli se jedna o absolutni cestu, a pod...)
    *
    * @param    &str Retezec reprezentujici cestu
    */
    void set(const std::string& str);

    /* OPERATORS */

    /**
    * Pretezovani prirazovaciho operatoru
    *
    * @param    &path Novy Path
    *
    * @return   Path
    */
    Path &operator=(const Path& path);

    /**
    *
    *
    * @param
    *
    * @return
    */
    Path &operator=(Path &&path);

    /**
    * Overi jestli jsou si cesty rovny
    *
    * @param    Jina cesta
    *
    * @return   TRUE pokud si cesty rovny jsou, jinak FALSE
    */
    bool operator==(const Path &p);

    /**
    * Overi jestli jsou si cesty nejsou rovny
    *
    * @param    Jina cesta
    *
    * @return   TRUE pokud si cesty rovny nejsou, jinak FALSE
    */
    bool operator!=(const Path &p);

    /**
    * Slucuje dve cesty do jedne
    *
    * @note     Pridavana cesta nesmi byt absolutni
    *
    * @param    &other Pridavana cesta
    *
    * @return   Path Nova, sloucena cesta
    */
    Path operator+(const Path& other);

    /**
    * Pretezovany operator << pro snadnejsi vypis cesty na vystupni potok
    *
    * @param    &os Vystupni potok
    *
    * @return   std::ostream&
    */
    std::ostream& operator<<(std::ostream& os);

    /* ITERATORS */

    /**
    * Vrati zacatek iteratoru
    *
    * @return   std::vector<std::string>::iterator
    */
    std::vector<std::string>::iterator begin();

    /**
    * Vrati konec iteratoru
    *
    * @return   std::vector<std::string>::iterator
    */
    std::vector<std::string>::iterator end();

protected:

    /**
    * Rozdeli retezec reprezentujici cestu na segmenty podle separatoru (lomitko '/')
    *
    * @param    string Retezec reprezentujici cestu
    *
    * @return   std::vector<std::string> Kontejner segmentu cesty
    */
    std::vector<std::string> tokenize(const std::string &string);

    std::vector<std::string> m_path; /*!< Kontejner segmentu cesty */
    bool m_absolute; /*!< Je to absolutni cesta (zacina lomitkem '/')?  */
};


}

