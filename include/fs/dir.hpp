/*
    Copyright (c) 2017, Denis Colesnicov <eugustus@gmail.com>
    All rights reserved.
    New BSD License can be found in the LICENSE file.
*/

#pragma once

#include "info.hpp"

namespace fs
{

/**
* Vytvorit slozku/y
*
* @param    path cesta/y ke slozce
* @param    name nazev slozky
* @param    recursive Vytvorit slozky rekurzivne
* @param    err Ukazatel na kod chyby @see errno.hpp
*
* @return   FileInfo* Ukazatel na strukturu informaci o slozce nebo nullptr pokud se nepovedlo
*/
FileInfo* dirCreate(Path path, bool recursive = true, int* err = nullptr);

/**
* Precte obsah slozky
*
* @param    dir Ukazatel na strukturu informaci o slozce
* @param    hidden Zahrnout i skryte soubory/slozky
* @param    dots Zahrnout i (..|.) - jsou "symbolicke" odkazy na nadrazeny a aktualni slozku
* @param    err Ukazatel na kod chyby @see errno.hpp
*
* @return   DirectoryContent* Ukazatel na obsah slozky nebo nullptr pokud se nepovedlo
*/
DirectoryContent* dirRead(FileInfo* dir, bool hidden = false, bool dots = false, int* err = nullptr);

/**
* Prejmenuje slozku
*
* @param    dir Ukazatel na strukturu informaci o slozce
* @param    name Novy nazev slozky
* @param    err Ukazatel na kod chyby @see errno.hpp
*
* @return   FileInfo* Ukazatel na prejmenovanou slozku, jinak nullptr pokud se nepovedlo
*/
FileInfo* dirRename(FileInfo* dir, const std::string& name, int* err = nullptr);

/**
*
*
* @param    dir Ukazatel na strukturu informaci o slozce
* @param    to Cil kam zkopirovat (slozka)
* @param    rewrite Pokud v destinaci existuje slozka se stejnym nazvem tak ji prepsat?
* @param    err Ukazatel na kod chyby @see errno.hpp
*
* @return   FileInfo* Ukazatel na zkopirovanou slozku, jinak nullptr pokud se nepovedlo
*
* @note     Mozna pridat moznost 'ignorovani chyb'... (prepsat, preskocit, ukoncit, a pod...)
* @note     Pokud se kopiruje do stejne slozky ktera se kopiruje, zpusolbuje to nekonecny cyklus kde se do nove slozky kopiruje ta sama slozka.
*/
FileInfo* dirCopy(FileInfo* dir, Path to, bool rewrite = false, int* err = nullptr);

/**
* Presune slozku na nove misto
*
* @param    dir Ukazatel na strukturu informaci o slozce
* @param    to Cil kam presunout (slozka)
* @param    rewrite Pokud v destinaci existuje slozka se stejnym nazvem tak ji prepsat?
* @param    err Ukazatel na kod chyby @see errno.hpp
*
* @return   FileInfo* Ukazatel na presunutou slozku, jinak nullptr pokud se nepovedlo
*/
FileInfo* dirMove(FileInfo* dir, Path to, bool rewrite = false, int* err = nullptr);

/**
* Smazat slozku (mozno i s obsahem)
*
* @param    dir Ukazatel na strukturu informaci o slozce
* @param    recursive Smazat adresar rekursivne (I pokud neni prazdny)?
* @param    err Ukazatel na kod chyby @see errno.hpp
*
* @return   TRUE pokud se povedlo, jinak FALSE
*
* @todo     Kdyz dojde ke smazani, mel by se i odstranit ukazatel na FileInfo*!!
*/
bool dirDelete(FileInfo* dir, bool recursive = false, int* err = nullptr);

/**
* Overi existenci slozky
*
* @param    path cesta ke slozce
* @param    err Ukazatel na kod chyby @see errno.hpp
*
* @return   TRUE pokud se povedlo, jinak FALSE
*/
bool dirExists(Path path, int* err = nullptr);

/**
* Je slozka prazdna?
*
* @param    path cesta ke slozce
* @param    err Ukazatel na kod chyby @see errno.hpp
*
* @return   TRUE pokud je prazdna, jinak FALSE
*/
bool dirEmpty(Path path, int* err = nullptr);

}

