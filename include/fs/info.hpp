/*
    Copyright (c) 2017, Denis Colesnicov <eugustus@gmail.com>
    All rights reserved.
    New BSD License can be found in the LICENSE file.
*/

#pragma once

#include "path.hpp"

#include <string>
#include <vector>
#include <ctime>


namespace fs
{

/**
* Typ "uzlu"
*/
enum Type
{
    UNKNOWN,    /*!< Neznamy */
    FILE,       /*!< Bezny soubor */
    DIRECTORY,  /*!< Bezna slozka */
    DEVICE,     /*!< Zarizeni (usb flash, oddil na disku, a pod...) */
    OTHER       /*!< Jine (socket, pipe, apod...) */
};

/**
* Opravneni k provadeni akci s "uzlem"
*/
enum Perm
{
    READ,/*!< Cteni */
    WRITE,/*!< Zapis */
    EXEC/*!< Spousteni */
};

/**
* Struktura informaci o "uzlu"
*/
struct FileInfo
{
    std::string name;/*!< Nazev "uzlu" */
    std::string path;/*!< Uplna cesta k "uzlu" */
    size_t bytes;/*!< Velikost "uzlu" v bytech */
    time_t created;/*!< Datum vytvoreni "uzlu" */
    time_t modified;/*!< Datum posledni upravy "uzlu" */
    time_t access;/*!< Datum posledniho pristupu "uzlu" */
    Type type;/*!< Typ "uzlu" */
    int uPerm;/*!< Opravneni pro USER k "uzlu" */
    int gPerm;/*!< Opravneni pro GROUP k "uzlu" */
    int rPerm;/*!< Opravneni pro ROOT k "uzlu" */
    long uid;/*!< ID vlastnika "uzlu" */
    long gid;/*!< ID skupiny vlastniku "uzlu" */
};

/**
* Struktura obsahu slozky.
* Obsahuje kontejnery structur informaci na slozky, soubory, zarizeni, a pod...
*/
struct DirectoryContent
{
    std::vector<FileInfo*> files;/*!< Bezne soubory */
    std::vector<FileInfo*> dirs;/*!< Slozky */
    std::vector<FileInfo*> others;/*!< Ostatni (zarizeni, sockets, pipes, a pod...) */
};

/**
* Vrati informaci o souboru/slozce
*
* @param    path cesta k souboru/slozce
* @param    err Ukazatel na kod chyby @see errno.hpp
*
* @return   FileInfo* ukazatel na strukturu informaci o souboru/slozce nebo nullptr pokud neexistuje
*
* @todo     pridat treti parametr pro chybu (int* errno)
*/
FileInfo* getInfo(Path path, int* err = nullptr);


}


