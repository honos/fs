/*
    Copyright (c) 2017, Denis Colesnicov <eugustus@gmail.com>
    All rights reserved.
    New BSD License can be found in the LICENSE file.
*/

#pragma once

#include "info.hpp"

#include <string>

namespace fs
{

/**
* Porovna prvni a druhou strukturu informaci o souboru/slozce podle velikosti
*
* @param first Reference na prvni strukturu informaci o souboru/slozce
* @param first Reference na druhou strukturu informaci o souboru/slozce
*
* @return TRUE pokud prvni je "vetsi" nez "druhe" jinak FALSE
*/
bool compareBySize( const FileInfo &left, const FileInfo &right );

/**
* Porovna prvni a druhou strukturu informaci o souboru/slozce podle data vytvoreni
*
* @param first Reference na prvni strukturu informaci o souboru/slozce
* @param first Reference na druhou strukturu informaci o souboru/slozce
*
* @return TRUE pokud prvni je "vetsi" nez "druhe" jinak FALSE
*/
bool compareByCreated( const FileInfo &left, const FileInfo &right );

/**
* Porovna prvni a druhou strukturu informaci o souboru/slozce podle cesty
*
* @param first Reference na prvni strukturu informaci o souboru/slozce
* @param first Reference na druhou strukturu informaci o souboru/slozce
*
* @return TRUE pokud prvni je "vetsi" nez "druhe" jinak FALSE
*/
bool compareByPath( const FileInfo &left, const FileInfo &right );

/**
* Porovna prvni a druhou strukturu informaci o souboru/slozce podle nazvu
*
* @param first Reference na prvni strukturu informaci o souboru/slozce
* @param first Reference na druhou strukturu informaci o souboru/slozce
*
* @return TRUE pokud prvni je "vetsi" nez "druhe" jinak FALSE
*/
bool compareByName(const FileInfo* first, const FileInfo* second );

/**
* Konvertuje bytovou hodnotu na citelnou podobu
*
* @details  Citelnejsi hodnota je (1.9 MB, 0.8 GB, 24 KB, apod...)
*
* @param    bytes Velikost v bytech
*
* @return   std::string Velikost v citelnejsi podobe
*/
std::string formatSizeUnits(size_t bytes);

}

