/*
    Copyright (c) 2017, Denis Colesnicov <eugustus@gmail.com>
    All rights reserved.
    New BSD License can be found in the LICENSE file.
*/

#pragma once

#include <string>

namespace fs
{

/**
* Chybove kody
* Jsou prevzate z error.h a jsou oriznuty o prvni pismenko 'E'
* Pocinaje kodem 200 a vys, jsou mnou pridane kody
*
* @note (E)2BIG je vymeneno za TOOBIG
*/
enum Error
{
    PERM            = 1,        /*!< Operation not permitted (rename, remove, mkdir) */
    NOENT           = 2,        /*!< No such file or directory (realpath, stat, rename, remove, mkdir, opendir) */
    IO              = 5,        /*!< I/O error (realpath, remove) */
    BADF            = 9,        /*!< Bad file number (stat, opendir, readdir, closedir) */
    NOMEM           = 12,       /*!< Out of memory (realpath, stat, rename, remove, mkdir, opendir) */
    ACCES           = 13,       /*!< Permission denied (realpath, stat, rename, remove, mkdir, opendir) */
    FAULT           = 14,       /*!< Bad address (stat, rename, remove, mkdir) */
    BUSY            = 16,       /*!< Device or resource busy (rename, remove) */
    EXIST           = 17,       /*!< File exists (rename, mkdir) */
    XDEV            = 18,       /*!< Cross-device link (rename) */
    NOTDIR          = 20,       /*!< Not a directory (realpath, stat, rename, remove, mkdir, opendir) */
    ISDIR           = 21,       /*!< Is a directory (rename, remove) */
    INVAL           = 22,       /*!< Invalid argument (realpath, getline, rename, remove) */
    NFILE           = 23,       /*!< File table overflow (opendir) */
    MFILE           = 24,       /*!< Too many open files (opendir) */
    NOSPC           = 28,       /*!< No space left on device (rename, mkdir) */
    ROFS            = 30,       /*!< Read-only file system (rename, remove, mkdir) */
    MLINK           = 31,       /*!< Too many links (rename, mkdir) */
    NAMETOOLONG     = 36,       /*!< File name too long (realpath, stat, rename, remove, mkdir) */
    NOTEMPTY        = 39,       /*!< Directory not empty (rename, remove) */
    LOOP            = 40,       /*!< Too many symbolic links encountered (realpath, stat, rename, remove, mkdir) */
    _OVERFLOW        = 75,       /*!< Value too large for defined data type (stat) */
    DQUOT           = 122,      /*!< Quota exceeded (rename, mkdir) */

// custom
    NOTFILE         = 200,      /*!< Not a file */
    NOTDEST         = 201       /*!< Destination directory not exists */

};

/**
* Nastavi kod chyby
*
* @param    *err Ukazatel na kod chyby
* @param    code Kod chyby
*/
void setError(int* err, int code);

/**
* Vrati smysluplny popis chyby
*
* @param    err Kod chyby
*
* @return   std::string popis chyby
*/
std::string tostring(int err);

}

